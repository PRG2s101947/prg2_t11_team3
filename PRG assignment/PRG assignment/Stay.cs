﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class Stay
    {
        public DateTime AdmittedDate { get; set; }
        public DateTime? DischargeDate { get; set; }

        public bool IsPaid { get; set; }
        public List<MedicalRecord> MedicalRecordsList { get; set; } = new List<MedicalRecord>();
        public List<BedStay> BedStayList { get; set; } = new List<BedStay>();
        public Patient Patient { get; set; }

        public Stay(DateTime admittedDate, Patient patient)
        {

            AdmittedDate = admittedDate;
            Patient = patient;
        }

        public void AddMedicalRecord(MedicalRecord medicalRecord)
        {
            MedicalRecordsList.Add(medicalRecord);
        }

        public void AddBedStay(BedStay bedStay)
        {
            BedStayList.Add(bedStay);
        }

        public override string ToString()
        {
            return "Admited date" + AdmittedDate + "Discharge Date" + DischargeDate + "Is Paid By" + IsPaid;


        }
    }
}
