﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class Adult : Patient
    {
        public double MedisaveBalance { get; set; }

        public Adult(string id, string name, int age, char gender, string citizenstat, string status, double medisaveBalance) : base(id, name, age, gender, citizenstat, status)
        {
            MedisaveBalance = medisaveBalance;

        }

        public override double CalculateCharges()
        {
            if (CitizenStatus == "PR" || CitizenStatus == "SC")
            {
                return base.CalculateCharges() - MedisaveBalance;
            }
            else
            {
                return base.CalculateCharges();
            }
            
        }

        public override string ToString()
        {
            return base.ToString() + "Medisave Balance" + MedisaveBalance;
        }

    }
}

