﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PRG_assignment
{
    class ClassBBed : Bed
    {
        public bool AirCon { get; set; }
        public ClassBBed(int wardNo, int bedNo, double dailyRate, bool available) : base(wardNo, bedNo, dailyRate, available)
        {

        }

        public override double CalculateCharges(string citizenStatus, int noOfDays)
        {
            if (citizenStatus == "SC")
            {
                if (AirCon == true)
                {
                    return ((base.DailyRate * noOfDays) * 0.7) + (50 * (Math.Ceiling(Convert.ToDouble(noOfDays / 7))));
                }
                else
                {
                    return ((base.DailyRate * noOfDays) * 0.7);
                }

            }
            else if (citizenStatus == "PR")
            {
                if (AirCon == true)
                {
                    return ((base.DailyRate * noOfDays) * 0.4) + (50 * (Math.Ceiling(Convert.ToDouble(noOfDays / 7))));
                }
                else
                {
                    return ((base.DailyRate * noOfDays) * 0.4);
                }
            }
            else
            {
                return (DailyRate * noOfDays) + (50 * (Math.Ceiling(Convert.ToDouble(noOfDays / 7))));
            }

        }

        public override string ToString()
        {
            return base.ToString() + "Class Bed B";
        }
    }
}
