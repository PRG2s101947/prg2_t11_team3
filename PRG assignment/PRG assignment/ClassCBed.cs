﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class ClassCBed : Bed
    {
        public bool PortableTv { get; set; }

        public ClassCBed(int wardNo, int bedNo, double dailyRate, bool available) : base(wardNo, bedNo, dailyRate, available)
        {

        }

        public override double CalculateCharges(string citizenStatus, int noOfDays)
        {
            if (citizenStatus == "SC")
            {
                if (PortableTv == true)
                {
                    return ((base.DailyRate * noOfDays) * 0.8) + 30;
                }
                else
                {
                    return ((base.DailyRate * noOfDays) * 0.8);
                }

            }
            else if (citizenStatus == "PR")
            {
                if (PortableTv == true)
                {
                    return ((base.DailyRate * noOfDays) * 0.6) + 30;
                }
                else
                {
                    return ((base.DailyRate * noOfDays) * 0.6);
                }
            }
            else
            {
                if (PortableTv == true)
                {
                    return ((base.DailyRate * noOfDays)) + 30;
                }
                else
                {
                    return ((base.DailyRate * noOfDays));
                }
            }
        }

        public override string ToString()
        {
            return base.ToString() + "Bed Class C";
        }
    }
}