﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PRG_assignment
{
    class Program
    {
        static void Main(string[] args)
        {

            //lists
            List<Patient> patientList = new List<Patient>();
            List<Stay> staylist = new List<Stay>();
            List<Bed> bedList = new List<Bed>();
            List<MedicalRecord> recordList = new List<MedicalRecord>();
            List<Pm25> pmList = new List<Pm25>();



            //while loop to run program
            bool runprogram = true;
            while (runprogram == true)
            {

                //menu
                Console.WriteLine("Menu");
                Console.WriteLine("====");
                Console.WriteLine("1.View all patients");
                Console.WriteLine("2.View all beds");
                Console.WriteLine("3.Register patient");
                Console.WriteLine("4.Add new bed");
                Console.WriteLine("5.Register a hospital stay");
                Console.WriteLine("6.Retrieve patient details");
                Console.WriteLine("7.add medical record entry");
                Console.WriteLine("8.View medical records");
                Console.WriteLine("9.Transfer patient to another bed");
                Console.WriteLine("10. Discharge and payment");
                Console.WriteLine("11. Display currencies exchange rate");
                Console.WriteLine("12. Display PM 2.5 information");
                Console.WriteLine("0. Exit");
                Console.Write("Enter your option: ");
                string option = Console.ReadLine();



                //Qn 2.1
                if (option == "1")
                {
                    Console.WriteLine("");
                    //title
                    Console.WriteLine("Option 1. View All Patients");

                    //Print the formatting
                    Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-20}{5,-15}", "Name", "ID No.", "Age", "Gender", "Citizenship", "Status");

                    //load patient list from file
                    InitPatientList(patientList);
                    //display the patient list
                    DisplayPatientList(patientList);
                    Console.WriteLine();


                }

                //Qn 2.2
                else if (option == "2")
                {
                    Console.WriteLine("");
                    //title
                    Console.WriteLine("Option 2. View All Beds");

                    //Load bed list from file
                    InitBedList(bedList);

                    //Display the bedList
                    DisplayBeds(bedList);
                    Console.WriteLine();

                }

                //2.3
                else if (option == "3")
                {
                    Console.WriteLine("");
                    //Title
                    Console.WriteLine("Option 3. Register Patient");



                    bool incorrect = true;

                    //loop to check for valid inputs
                    while (incorrect)
                    {
                        Console.Write("Enter Name: ");
                        //prompt for name
                        string option3name = Console.ReadLine();

                        //validate if input is correct
                        bool valid = CheckString(option3name);
                        if (valid == false)
                        {
                            Console.WriteLine("\nPlease enter a name.");
                        }
                        else
                        {
                            //if correct prompt for ID number
                            Console.Write("Enter Identification Number: ");
                            string option3ID = Console.ReadLine();

                            //validate ID number
                            valid = CheckvID(option3ID);
                            if (valid == false)
                            {
                                Console.WriteLine("\nInvalid ID entered.");
                            }
                            else
                            {
                                //if correct prmpt for Age
                                Console.Write("Enter Age: ");
                                string option3Age = Console.ReadLine();
                                //validate age
                                valid = Checkint(option3Age);

                                if (valid == false)
                                {
                                    Console.WriteLine("\nInvalid Age entered.");
                                }
                                else
                                {
                                    //if correct prompt for gender
                                    int age = Convert.ToInt32(option3Age);

                                    Console.Write("Enter Gender [M/F]: ");
                                    string option3Gender = Console.ReadLine().ToUpper();
                                    valid = CheckString(option3Gender);

                                    //validate if gender was correct
                                    if (option3Gender == "M" || option3Gender == "F")
                                    {
                                        Console.Write("Enter Citizenship Status [SC/PR/Foreinger]: ");
                                        string option3CitizenStat = Console.ReadLine().ToUpper();
                                        valid = CheckString(option3CitizenStat);
                                        if (option3CitizenStat == "SC" || option3CitizenStat == "PR" || option3CitizenStat == "FOREINGER")
                                        {
                                            if (RegisterNewPatient(option3name, option3ID, age, option3Gender, option3CitizenStat, patientList) == true)
                                            {
                                                using (StreamWriter file = new StreamWriter("patients(4).csv", append: true))
                                                {
                                                    string patientdeets = option3name + "," + option3ID + "," + option3Age + "," + option3Gender + "," + option3CitizenStat;


                                                    file.WriteLine(patientdeets);
                                                    patientList.Clear();

                                                    Console.ForegroundColor = ConsoleColor.Green;
                                                    Console.WriteLine("{0} is registered successfully.", option3name);
                                                    Console.ForegroundColor = ConsoleColor.White;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("\nInvalid amount");
                                            }

                                        }
                                        else
                                        {
                                            Console.WriteLine("\nInvalid Citizenship status entered.");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("\nInvalid Gender entered.");
                                    }
                                }
                            }
                        }
                    }
                    Console.WriteLine();
                }

                //Qn 2.4
                else if (option == "4")
                {
                    InitBedList(bedList);
                    Console.WriteLine("");
                    //title
                    Console.WriteLine("Option 4. Add new bed");
                    AddNewBed(bedList);

                }
                //2.5
                else if (option == "5")
                {
                    Console.WriteLine("");
                    //title
                    Console.WriteLine("Option 5. Register a hospital stay");
                    //load patient list incase user wants to open this first
                    InitPatientList(patientList);
                    //formatting
                    Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-20}{5,-15}", "Name", "ID No.", "Age", "Gender", "Citizenship", "Status");

                    DisplayUnadmittedPatients(patientList);

                    bool incorrect = true;

                    //valdiation loop for inputs
                    while (incorrect)
                    {
                        Console.Write("Enter patient ID Number: ");
                        string option5ID = Console.ReadLine();
                        bool valid = CheckID(option5ID, patientList);
                        if (valid == true)
                        {
                            Patient p = SearchPatient(patientList, option5ID);
                            while (p is null)
                            {
                                Console.WriteLine("Invalid ID entered please try again");
                            }
                            Patient option5Patient = FindPatientID(patientList, option5ID);

                            Console.WriteLine();
                            InitBedList(bedList);
                            DisplayBeds(bedList);

                            Console.Write("Please select an available bed: ");
                            string option5bedIndex = Console.ReadLine();
                            valid = Checkint(option5bedIndex);
                            if (valid == true && Convert.ToInt32(option5bedIndex) < (bedList.Count() + 1) && FindBedIndex(bedList, option5bedIndex).Available == true)
                            {
                                Bed option5Bed = FindBedIndex(bedList, option5bedIndex);
                                Console.Write("Please enter the date of admission: ");
                                string admissionDate = Console.ReadLine();
                                valid = CheckDateTime(admissionDate);
                                if (valid == true)
                                {
                                    DateTime date = Convert.ToDateTime(admissionDate);
                                    if (option5Bed is ClassABed)
                                    {
                                        //cast for child class
                                        ClassABed i = (ClassABed)option5Bed;
                                        Console.Write("Any accompanying guest?(Additional $100 per day) [Y/N]: ");
                                        string additionalAddOn = Console.ReadLine();
                                        if (additionalAddOn == "Y" || additionalAddOn == "N")
                                        {
                                            i.AccompanyingPerson = true;
                                        }
                                    }
                                    else if (option5Bed is ClassBBed)
                                    {
                                        //cast for child class
                                        ClassBBed i = (ClassBBed)option5Bed;
                                        Console.Write("Upgrade to an aircon room?(Additional $50 per week) [Y/N]: ");
                                        string additionalAddOn = Console.ReadLine();
                                        if (additionalAddOn == "Y" || additionalAddOn == "N")
                                        {
                                            i.AirCon = true;
                                        }
                                    }
                                    else if (option5Bed is ClassCBed)
                                    {
                                        //cast for child class
                                        ClassCBed i = (ClassCBed)option5Bed;
                                        Console.Write("Rent a portable TV?(Additional $30) [Y/N]: ");
                                        string additionalAddOn = Console.ReadLine();
                                        if (additionalAddOn == "Y" || additionalAddOn == "N")
                                        {
                                            i.PortableTv = true;
                                        }
                                    }


                                    //make stay obj
                                    Stay newStay = new Stay(date, option5Patient);

                                    //make bedstay obj to be put into patient
                                    BedStay newBedStay = new BedStay(date, option5Bed, Convert.ToDateTime(null));
                                    newStay.BedStayList.Add(newBedStay);

                                    option5Patient.Status = "Admitted";
                                    option5Patient.Stay = newStay;
                                    option5Bed.Available = false;


                                    //check if status is admitted
                                    foreach (Patient i in patientList)
                                    {
                                        if (i == option5Patient)
                                        {
                                            i.Status = "Admitted";
                                            i.Stay = newStay;

                                        }
                                    }

                                    Console.WriteLine();
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("Stay registration successful!");
                                    Console.ForegroundColor = ConsoleColor.White;
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("\nInvalid admission entered");
                                }

                            }
                            else
                            {
                                Console.WriteLine("\nInvalid Bed entered.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("\nInvalid ID entered.");
                        }
                    }
                }


                //2.6
                else if (option == "6")
                {

                    Console.WriteLine("");
                    Console.WriteLine("Option 6.Retrieve Patient Details");

                    bool incorrect = true;

                    //validation loop
                    while (incorrect)
                    {

                        //load file incase user wants to open this first
                        InitPatientList(patientList);

                        //formatting
                        Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-20}{5,-15}", "Name", "ID No.", "Age", "Gender", "Citizenship", "Status");
                        DisplayPatientList(patientList);

                        //prompt for user ID
                        Console.Write("Please enter your patient ID: ");

                        string option6ID = Console.ReadLine();
                        bool valid = CheckID(option6ID, patientList);
                        if (valid == true)
                        {
                            if (SearchPatient(patientList, option6ID) == null)
                            {
                                Console.WriteLine("Please enter a valid ID");
                            }
                            else
                            {
                                Console.WriteLine();
                                DisplayPatientInfo(patientList, option6ID);
                                break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("\nInvalid ID entered");
                        }
                    }
                }

                //Qn 2.7
                else if (option == "7")
                {
                    Console.WriteLine("");
                    //Load file incase user wants to open this option first
                    InitPatientList(patientList);

                    //display patients
                    DisplayAdmittedPatients(patientList);
                    Console.WriteLine("Option 7.Add Medical Entry");
                    AddRecord(patientList);

                }

                else if (option == "8")
                {
                    Console.WriteLine("");
                    //2.8
                    Console.WriteLine("Option 8. View medical records");
                    InitPatientList(patientList);
                    DisplayAdmittedPatients(patientList);
                    ViewRecords(patientList);

                }

                else if (option == "9")
                {
                    Console.WriteLine("");
                    Console.WriteLine("Option 9. Transfer Patient to Another Bed");
                    //2.9
                    InitPatientList(patientList);
                    DisplayAdmittedPatients(patientList);

                    Console.Write("Enter patient ID number: ");
                    string option9ID = Console.ReadLine();

                    if (option9ID.Length == 9)
                    {
                        Stay option9StayItem = RetrieveStayObject(patientList, option9ID);

                        //get current bed item 

                        DisplayBeds(bedList);

                        Console.Write("Please choose an available bed: ");
                        string option9BedIndex = Console.ReadLine();
                        if (Convert.ToInt32(option9BedIndex) > bedList.Count() && Checkint(option9BedIndex) == true && (FindBedIndex(bedList, option9BedIndex).Available == false))
                        {
                            Console.WriteLine("Invalid bed selected please try again");
                        }
                        else
                        {
                            Bed option9bedItem = (FindBedIndex(bedList, option9BedIndex));
                            if (option9bedItem is ClassABed)
                            {
                                ClassABed i = (ClassABed)option9bedItem;
                                Console.Write("Any accompanying guest?(Additional $100 per day) [Y/N]: ");
                                string additionalAddOn = Console.ReadLine();
                                if (additionalAddOn == "Y" || additionalAddOn == "N")
                                {
                                    i.AccompanyingPerson = true;
                                }


                            }
                            else if (option9bedItem is ClassBBed)
                            {
                                ClassBBed i = (ClassBBed)option9bedItem;
                                Console.Write("Upgrade to an aircon room?(Additional $50 per week) [Y/N]: ");
                                string additionalAddOn = Console.ReadLine();
                                if (additionalAddOn == "Y" || additionalAddOn == "N")
                                {
                                    i.AirCon = true;
                                }
                            }
                            else if (option9bedItem is ClassCBed)
                            {
                                ClassCBed i = (ClassCBed)option9bedItem;
                                Console.Write("Rent a portable TV?(Additional $30) [Y/N]: ");
                                string additionalAddOn = Console.ReadLine();
                                if (additionalAddOn == "Y" || additionalAddOn == "N")
                                {
                                    i.PortableTv = true;
                                }
                            }


                            Console.Write("Please enter the date of transfer: ");
                            DateTime option9timetransfer = Convert.ToDateTime(Console.ReadLine());

                            foreach (BedStay i in option9StayItem.BedStayList)
                            {
                                Bed currentbed = i.Bed;
                                currentbed.Available = true;
                                i.EndBedStay = option9timetransfer;
                            }


                            BedStay bs = new BedStay(option9timetransfer, option9bedItem, Convert.ToDateTime(null));

                            option9StayItem.BedStayList.Add(bs);
                            option9bedItem.Available = false;
                            Console.WriteLine();


                            Console.WriteLine("Patient will be transferred to Ward {0} Bed {1} on {2}",option9bedItem.WardNo,option9bedItem.BedNo, option9timetransfer);
                        }


                    }
                    else
                    {
                        Console.WriteLine("\tInvalid ID entered");
                    }
                }

                else if (option == "10")
                {
                    try
                    {
                        Console.WriteLine("");
                        //2.10
                        InitPatientList(patientList);
                        DisplayAdmittedPatients(patientList);

                        Console.Write("Patient ID: ");
                        string option10ID = Console.ReadLine();
                        Patient p = SearchPatient(patientList, option10ID);
                        while (p is null)
                        {
                            Console.Write("Please Enter Valid ID: ");
                            option10ID = Console.ReadLine();
                            p = SearchPatient(patientList, option10ID);
                        }

                        Console.Write("Please enter the date of discharge (dd/mm/yyyy): ");
                        DateTime option10Datetime = Convert.ToDateTime(Console.ReadLine());
                        Patient option10Patient = FindPatientID(patientList, option10ID);
                        option10Patient.Stay.DischargeDate = option10Datetime;
                        option10Patient.Stay.BedStayList[option10Patient.Stay.BedStayList.Count - 1].EndBedStay = option10Datetime;

                        Console.WriteLine();
                        Console.WriteLine();

                        Console.WriteLine("Name of patient: {0}", p.Name);
                        Console.WriteLine("ID number: {0}", p.Id);
                        Console.WriteLine("Citizenship Status: {0}", p.CitizenStatus);
                        Console.WriteLine("Gender: {0}", p.Gender);
                        Console.WriteLine("Status: Discharged");

                        DisplayPatientInfo(patientList, option10ID);

                        //find bed
                        Bed Option10Bed = FindBedByID(bedList, option10ID, patientList);
                        Option10Bed.Available = true;

                        DisplayBedStayDeets(bedList, option10ID, patientList);



                        double option10charge = (CalculatePatientCharges(p, patientList, bedList));


                        //calculate and display the the total unpaid hospital charges
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }

                }

                else if (option == "11")
                {
                    //2.11
                    DisplayXchange();
                }

                else if (option == "12")
                {   
                    //2.12
                    DisplayPM(pmList);
                }

                else if (option == "0")
                {
                    runprogram = false;
                    break;
                }
                else
                {
                    Console.WriteLine("\tOption selected invalid. Please try again");
                    continue;
                }
            }

        }


        //Validation Methods

        //check valid string input
        static bool CheckString(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return false;
            }
            else
            {
                return true;
            }
        }



        //Check for valid ID input

        static bool CheckvID(string id)
        {
            if (id.Length == 9)
            {
                if (Char.IsLetter(id[0]) == true)
                {
                    if (Char.IsLetter(id[8]) == true)
                    {
                        if (id.Substring(1, 7).All(Char.IsDigit) == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        static bool CheckID(string id, List<Patient> pList)
        {
            foreach (Patient i in pList)
            {
                if (i.Id == id)
                {
                    return true;
                }
                else
                {
                    continue;
                }
            }
            return false;

        }

        //Check vaild double
        static bool CheckDouble(string amt)
        {
            double aDouble;
            if (amt is null)
            {
                return false;
            }

            bool valid = double.TryParse(amt, out aDouble);

            return valid;
        }

        //method to check for valid date time
        static bool CheckDateTime(string time)
        {
            DateTime date;
            if (time is null)
            {
                return false;
            }
            if (time.Contains("/") == true)
            {
                if (DateTime.TryParseExact(time, "dd/mm/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out date))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }



        //Method to check for patients admitted
        static Patient SearchPatient(List<Patient> pList, string patientID)
        {
            foreach (Patient p in pList)
            {
                if (p.Id == patientID)
                {
                    return p;
                }
            }

            return null;

        }

        //check valid integer
        static bool Checkint(string z)
        {
            int check;
            if (int.TryParse(z, out check))
            {
                if (check <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }



        static Bed SearchBed(List<Bed> bList, int WardNo)
        {
            foreach (Bed b in bList)
            {
                if (b.WardNo == WardNo)
                {
                    return b;
                }

            }
            return null;
        }




        //2.1
        static void InitPatientList(List<Patient> patientsList)
        {
            if (patientsList.Count > 0)
            {
                Console.WriteLine("");
            }
            else
            {
                //read patient list
                string[] lines = File.ReadAllLines("patients(4).csv");
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] data = lines[i].Split(',');

                    if (Convert.ToInt32(data[2]) < 13)
                    {
                        if (data.Length == 6)
                        {
                            patientsList.Add(new Child(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "\tRegistered", Convert.ToDouble(data[5])));

                        }
                        else
                        {
                            patientsList.Add(new Child(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "\tRegistered", Convert.ToDouble(null)));

                        }
                    }
                    else if ((Convert.ToInt32(data[2]) > 12) && (Convert.ToInt32(data[2]) < 65))
                    {
                        if (data.Length == 6)
                        {
                            patientsList.Add(new Adult(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "\tRegistered", Convert.ToDouble(data[5])));

                        }
                        else
                        {
                            patientsList.Add(new Adult(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "\tRegistered", Convert.ToDouble(null)));

                        }
                    }
                    else if (Convert.ToInt32(data[2]) > 64)
                    {

                        patientsList.Add(new Senior(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "\tRegistered"));


                    }



                }
            }

        }

        //2.2
        static void InitBedList(List<Bed> bList)
        {
            if (bList.Count > 0)
            {
                Console.WriteLine("");
            }
            else
            {
                //read bed list
                string[] lines = File.ReadAllLines("beds.csv");
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] data = lines[i].Split(',');
                    bool availabilitycheck = true;
                    if (data[3] == "No")
                    {
                        availabilitycheck = false;
                    }


                    if (data[0] == "A")
                    {
                        bList.Add(new ClassABed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToInt32(data[4]), availabilitycheck));
                    }
                    else if (data[0] == "B")
                    {
                        bList.Add(new ClassBBed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToInt32(data[4]), availabilitycheck));


                    }
                    else if (data[0] == "C")
                    {
                        bList.Add(new ClassCBed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToInt32(data[4]), availabilitycheck));
                    }



                }
            }



        }

        //2.2
        static void DisplayBeds(List<Bed> bedList)
        {


            Console.WriteLine("{0,-15} {1,-15} {2,-15} {3,-15} {4,-15} {5,-15}",
                "No", "Type", "Ward No", "Bed No", "Available", "Daily Rate");

            //display bed
            int bedIndex = 1;
            foreach (Bed i in bedList)
            {
                if (i is ClassABed)
                {
                    Console.WriteLine("{0,-15} {1,-15} {2,-15} {3,-15} {4,-15} {5,-15}", bedIndex, "A", i.WardNo, i.BedNo, Convert.ToString(i.Available), i.DailyRate);
                    bedIndex++;
                }
                else if (i is ClassBBed)
                {
                    Console.WriteLine("{0,-15} {1,-15} {2,-15} {3,-15} {4,-15} {5,-15}", bedIndex, "B", i.WardNo, i.BedNo, Convert.ToString(i.Available), i.DailyRate);
                    bedIndex++;
                }
                else if (i is ClassCBed)
                {
                    Console.WriteLine("{0,-15} {1,-15} {2,-15} {3,-15} {4,-15} {5,-15}", bedIndex, "C", i.WardNo, i.BedNo, Convert.ToString(i.Available), i.DailyRate);
                    bedIndex++;
                }

            }

        }

        static Bed FindBedIndex(List<Bed> bList, string index)
        {
            try
            {
                return (bList[Convert.ToInt32(index) - 1]);
            }
            catch
            {
                Console.WriteLine("Cannot find bed from index please retry");

                return null;
            }
        }

        //2.3
        static bool RegisterNewPatient(string option3name, string option3ID, int option3Age, string option3Gender, string option3CitizenStat, List<Patient> patientList)
        {
            //check for payment later on
            if (option3Age < 13 && option3CitizenStat == "SC")
            {
                //validate double figure
                Console.Write("Please enter your Child development Account(CDA) Balance: ");
                string option3DCABAL = Console.ReadLine();
                if (CheckDouble(option3DCABAL) == true)
                {
                    patientList.Add(new Child(option3ID, option3name, option3Age, Convert.ToChar(option3Gender), option3CitizenStat, "Registered", Convert.ToDouble(option3DCABAL)));
                    return true;
                }
                else
                {
                    Console.WriteLine("Please enter a valid balance");
                    return false;
                }

            }
            else if (option3Age > 12 && option3Age < 65 && (option3CitizenStat == "SC" || option3CitizenStat == "PR"))
            {
                //Validate double figure
                Console.Write("Please enter your medisave balance: ");
                string option3MedisaveBAL = (Console.ReadLine());
                if (CheckDouble(option3MedisaveBAL) == true)
                {
                    patientList.Add(new Adult(option3ID, option3name, option3Age, Convert.ToChar(option3Gender), option3CitizenStat, "Registered", Convert.ToDouble(option3MedisaveBAL)));
                    return true;
                }
                else
                {
                    Console.WriteLine("invalid input please try again");
                    return false;
                }
            }

            else if (option3Age < 13)
            {
                //add ppatient to list
                if (option3CitizenStat != "SC")
                {
                    patientList.Add(new Child(option3ID, option3name, option3Age, Convert.ToChar(option3Gender), option3CitizenStat, "Registered", 0.0));
                    return true;
                }
            }
            else if (option3Age > 12 && option3Age < 65 && (option3CitizenStat != "SC" && option3CitizenStat != "PR"))
            {
                patientList.Add(new Adult(option3ID, option3name, option3Age, Convert.ToChar(option3Gender), option3CitizenStat, "Registered", 0));
                return true;
            }
            else if (option3Age > 64)
            {
                patientList.Add(new Senior(option3ID, option3name, option3Age, Convert.ToChar(option3Gender), option3CitizenStat, "Registered"));
                return true;
            }
            return false;
        }

        static void DisplayPatientInfo(List<Patient> pList, string ID)
        {
            try
            {
                //display patient info
                foreach (Patient i in pList)
                {
                    if (i.Id == ID)
                    {
                        Console.WriteLine("Name of patient: " + i.Name);
                        Console.WriteLine("ID Number: " + i.Id);
                        Console.WriteLine("Citizenship status: " + i.CitizenStatus);
                        Console.WriteLine("Gender: " + i.Gender);
                        Console.WriteLine("Status:" + i.Status);


                        if (i.Stay == null)
                        {
                            Console.WriteLine();
                            Console.WriteLine("Patient is unadmitted");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Admission date: " + i.Stay.AdmittedDate);
                            Console.WriteLine("Discharge date: " + i.Stay.DischargeDate);
                            Console.WriteLine("Payment status: " + i.Stay.IsPaid);
                            Console.WriteLine("======");
                            Console.WriteLine("Ward Number: " + i.Stay.BedStayList[0].Bed.WardNo);
                            Console.WriteLine("Bed Number: " + i.Stay.BedStayList[0].Bed.BedNo);
                        }
                        Console.WriteLine();

                        //check ward type
                        if (i is ClassABed)
                        {
                            Console.WriteLine("Ward Class: A");
                        }
                        else if (i is ClassBBed)
                        {
                            Console.WriteLine("Ward Class: B");
                        }
                        else if (i is ClassCBed)
                        {
                            Console.WriteLine("Ward Class: C");
                        }

                        Console.WriteLine("Start of bed stay: " + i.Stay.BedStayList[0].StartBedStay);
                        Console.WriteLine("End of bed stay: " + i.Stay.BedStayList[0].EndBedStay);

                    }



                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void DisplayAvailBeds(List<Bed> bedList)
        {
            //check if bed available
            foreach (Bed i in bedList)
            {
                if (i.Available == true)

                {
                    int q = bedList.IndexOf(i);
                    Console.Write(q);
                    i.ToString();
                }
            }
        }

        static void DisplayPatientList(List<Patient> patientList)
        {

            foreach (Patient i in patientList)
            {
                Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-15}", i.Name, i.Id, i.Age, i.Gender, i.CitizenStatus, i.Status);
            }
        }

        static void DisplayUnadmittedPatients(List<Patient> pList)
        {
            foreach (Patient i in pList)
            {
                if (i.Status != "Admitted")
                {
                    Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-15}", i.Name, i.Id, i.Age, i.Gender, i.CitizenStatus, i.Status);
                }
            }
        }

        //2.9
        static void DisplayAdmittedPatients(List<Patient> pList)
        {
            Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-15}", "Name", "ID No.", "Age", "Gender", "Citizenship", "Status");
            foreach (Patient i in pList)
            {
                if (i.Status == "Admitted")
                {
                    Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}{5,-15}", i.Name, i.Id, i.Age, i.Gender, i.CitizenStatus, i.Status);
                }
            }
        }

        //2.9
        static Stay RetrieveStayObject(List<Patient> pList, string ID)
        {
            foreach (Patient i in pList)
            {
                if (ID == i.Id)
                {
                    return i.Stay;
                }
            }
            return null;
        }

        static Patient FindPatientID(List<Patient> pList, string ID)
        {
            foreach (Patient i in pList)
            {
                if (i.Id == ID)
                {
                    return i;
                }
                else
                {
                    continue;
                }
            }
            return null;
        }

        //2.4
        static void AddNewBed(List<Bed> bedList)
        {
            double aDouble;
            bool aBool;
            bool incorrect = true;

            while (incorrect)
            {
                Console.Write("Enter Ward Type [A/B/C]: ");//prompt user for info
                string WardType = Console.ReadLine();

                if (WardType == "A" || WardType == "B" || WardType == "C")
                {
                    Console.Write("Enter Ward No: ");
                    string WardNo = Console.ReadLine();
                    bool valid = Checkint(WardNo);//valid integer
                    if (valid == true)
                    {
                        incorrect = false;
                        int wNo = Convert.ToInt32(WardNo);

                        Console.Write("Enter Bed No: ");
                        string BedNo = Console.ReadLine();
                        valid = Checkint(BedNo);//validate integer
                        if (valid == true)
                        {
                            incorrect = false;
                            int bNo = Convert.ToInt32(BedNo);

                            Console.Write("Enter Daily Rate: $");
                            string rate = Console.ReadLine();
                            if (!double.TryParse(rate, out aDouble))//validte double
                            {
                                Console.WriteLine("\nInvalid rate entered");
                            }
                            else
                            {
                                double r = Convert.ToDouble(rate);
                                incorrect = false;

                                Console.Write("Available [Y/N]: ");
                                string available = Console.ReadLine();

                                if (available == "Y")
                                {
                                    string avail = "true";
                                    if (bool.TryParse(avail, out aBool))
                                    {
                                        incorrect = false;
                                        bool a = Convert.ToBoolean(avail);

                                        //add bed into bedlist
                                        using (StreamWriter file = new StreamWriter("beds.csv", true))
                                        {
                                            if (WardType == "A")
                                            {
                                                Bed b = new ClassABed(wNo, bNo, r, a);
                                                bedList.Add(b);
                                                string bed = "A" + "," + wNo + "," + bNo + "," + a + "," + r;
                                                file.WriteLine(bed);
                                                Console.WriteLine("\nBed added successfully.");
                                            }
                                            else if (WardType == "B")
                                            {
                                                Bed b = new ClassBBed(wNo, bNo, r, a);
                                                bedList.Add(b);
                                                string bed = "B" + "," + wNo + "," + bNo + "," + a + "," + r;
                                                file.WriteLine(bed);
                                                Console.WriteLine("\nBed added successfully.");
                                            }
                                            else if (WardType == "C")
                                            {
                                                Bed b = new ClassCBed(wNo, bNo, r, a);
                                                bedList.Add(b);
                                                string bed = "C" + "," + wNo + "," + bNo + "," + a + "," + r;
                                                file.WriteLine(bed);
                                                Console.WriteLine("\nBed added successfully.");
                                                Console.ForegroundColor = ConsoleColor.White;
                                            }
                                        }
                                    }

                                }

                                else if (available == "N")
                                {
                                    string avail = "false";
                                    if (bool.TryParse(avail, out aBool))//validate bool
                                    {
                                        incorrect = false;
                                        bool a = Convert.ToBoolean(avail);

                                        Console.ForegroundColor = ConsoleColor.Green;

                                        if (WardType == "A")
                                        {
                                            Bed b = new ClassABed(wNo, bNo, r, a);
                                            bedList.Add(b);
                                            Console.WriteLine("\nBed added successfully.");
                                        }
                                        else if (WardType == "B")
                                        {
                                            Bed b = new ClassBBed(wNo, bNo, r, a);
                                            bedList.Add(b);
                                            Console.WriteLine("\nBed added successfully.");
                                        }
                                        else if (WardType == "C")
                                        {
                                            Bed b = new ClassCBed(wNo, bNo, r, a);
                                            bedList.Add(b);
                                            Console.WriteLine("\nBed added successfully.");
                                        }

                                        Console.ForegroundColor = ConsoleColor.White;
                                    }

                                }

                                else
                                {
                                    Console.WriteLine("\nInvalid availabiliy entered.");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("\nInvalid Bed No entered");
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nInvalid Ward No entered");
                    }
                }
                else
                {
                    Console.WriteLine("\nInvalid Ward Type entered");

                }
            }

        }

        //2.7
        static void AddRecord(List<Patient> patientList)
        {
            bool incorrect = true;

            while (incorrect)//while loop
            {
                Console.Write("Enter Patient Number: ");//prompt user info
                string pID = (Console.ReadLine());
                bool valid = CheckID(pID, patientList);//validate id
                if (valid == true)
                {
                    incorrect = false;
                    Patient p = SearchPatient(patientList, pID);
                    while (p is null)
                    {
                        Console.Write("Please enter a valid ID: ");
                        pID = (Console.ReadLine());
                        p = SearchPatient(patientList, pID);

                    }

                    Console.Write("Enter patient temperature: ");
                    string newTempString = Console.ReadLine();
                    double newTemp = 0;
                    while (!double.TryParse(newTempString, out newTemp))//validate double
                    {
                        Console.Write("Please enter a valid temperature: ");
                        newTempString = Console.ReadLine();
                    }

                    double nTemp = Convert.ToDouble(newTemp);
                    Console.Write("Enter patient diagnosis: ");
                    string Diagnosis = Console.ReadLine();

                    DateTime dateTimeEntered = DateTime.Now;

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Medical record entry for Adrian successful!");
                    Console.ForegroundColor = ConsoleColor.White;

                    p.Stay.MedicalRecordsList.Add(new MedicalRecord(Diagnosis, nTemp, dateTimeEntered));
                    break;
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid ID entered.");
                }
            }



        }

        //2.8
        static void ViewRecords(List<Patient> patientList)
        {
            Console.Write("Enter patient ID number: ");//prompt user for info
            string id = Console.ReadLine();

            Patient p = SearchPatient(patientList, id);//check if patient in patient list
            while (p is null)
            {
                Console.Write("Please enter a valid ID");
                id = Console.ReadLine();
            }

            Console.WriteLine("Name of patient: {0}" +
                            "\nID number: {1}" +
                            "\nCitizenship status: {2}" +
                            "\nGender: {3}" +
                            "\nStatus: {4}", p.Name, p.Id, p.CitizenStatus, p.Gender, p.Status);

            Console.WriteLine();
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("======Stay======");
            Console.WriteLine("Admission date: {0}" +
            "\nDischarge date: ", p.Stay.AdmittedDate);


            Console.WriteLine();
            Console.WriteLine();

            int i = 1;
            foreach (MedicalRecord r in p.Stay.MedicalRecordsList)
            {

                Console.WriteLine("======Record # {0}======", i);

                Console.WriteLine("Date/Time: {0}" +
                "\nTemperature: {1} deg. cel." +
                "\nDiagnosis: {2}", r.DatetimeEntered, r.Temperature, r.Diagnosis);

                Console.WriteLine("");

                i += 1;
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        //2.10
        static void DischargePayment(List<Patient> patientList, List<Bed> bedList, List<BedStay> stayList)
        {
            //display patient infor
            Console.WriteLine("======Bed #1 ======");
            foreach (Bed b in bedList)
            {
                Console.WriteLine("Ward number: " +
                    "\nBed number: " +
                    "\nWard Class: " +
                    "\nStart of bed stay: " +
                    "\nEnd of bed stay: " +
                    "\nAccompany person: ");
            }
        }

        //2.10
        static Bed FindBedByID(List<Bed> bedList, string ID, List<Patient> patientList)
        {

            Patient p = FindPatientID(patientList, ID);
            //find bed of patient
            foreach (BedStay bs in p.Stay.BedStayList)
            {
                return bs.Bed;
            }

            return null;
        }

        static void DisplayBedStayDeets(List<Bed> bedList, string ID, List<Patient> patientList)
        {
            //display
            Patient p = FindPatientID(patientList, ID);
            Console.WriteLine("=======Stay=======");

            Console.WriteLine(p.Stay.BedStayList[0].StartBedStay);
            Console.WriteLine(p.Stay.BedStayList[p.Stay.BedStayList.Count() - 1].EndBedStay);

            //check for payment
            if (p.Stay.IsPaid == false)
            {
                Console.WriteLine("Unpaid");
            }
            else
            {
                Console.WriteLine("Paid");
            }
            int bedindex = 0;
            foreach (BedStay bs in p.Stay.BedStayList)
            {
                //display nfor base on ward type
                bedindex++;
                Console.WriteLine("Number of days stayed {0}", Convert.ToInt32((Convert.ToDateTime(bs.EndBedStay) - bs.StartBedStay).TotalDays));
                Console.WriteLine("======Bed #  {0} ======", bedindex);
                Console.WriteLine("Ward Number: {0}", bs.Bed.WardNo);
                Console.WriteLine("Bed number: {0}", bs.Bed.BedNo);

                if (bs.Bed is ClassABed)
                {
                    Console.WriteLine("Ward Class: {0}", "A");
                    Console.WriteLine("Start of bed stay: {0}", bs.StartBedStay);
                    Console.WriteLine("End of bed stay: {0}", bs.EndBedStay);
                    ClassABed q = (ClassABed)bs.Bed;
                    Console.WriteLine("Accompanying person: {0}", q.AccompanyingPerson);
                }
                else if (bs.Bed is ClassBBed)
                {
                    Console.WriteLine("Ward Class: {0}", "B");
                    Console.WriteLine("Start of bed stay: {0}", bs.StartBedStay);
                    Console.WriteLine("End of bed stay: {0}", bs.EndBedStay);
                    ClassBBed q = (ClassBBed)bs.Bed;
                    Console.WriteLine("Accompanying person: {0}", q.AirCon);
                }
                else if (bs.Bed is ClassCBed)
                {
                    Console.WriteLine("Ward Class: {0}", "C");
                    Console.WriteLine("Start of bed stay: {0}", bs.StartBedStay);
                    Console.WriteLine("End of bed stay: {0}", bs.EndBedStay);
                    ClassCBed q = (ClassCBed)bs.Bed;
                    Console.WriteLine("Accompanying person: {0}", q.PortableTv);
                }
            }


        }

        static double CalculatePatientCharges(Patient q, List<Patient> patientList, List<Bed> bedList)
        {
            double charges = 0.0;

            //calculate charges base on age group
            if (q is Child)
            {
                Child c = (Child)q;
                return charges = c.CalculateCharges();
            }
            else if (q is Adult)
            {
                Adult a = (Adult)q;
                return charges = a.CalculateCharges();
            }
            else if (q is Senior)
            {
                Senior s = (Senior)q;
                return charges = s.CalculateCharges();
            }
            else
            {
                return charges;
            }



        }
        //advanced feature
        //3.2
        static void DisplayPM(List<Pm25> pmList)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.data.gov.sg");

                // HTTP GET
                Task<HttpResponseMessage> responseTask
                    = client.GetAsync("/v1/environment/pm25");
                responseTask.Wait();

                // Response
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    string data = readTask.Result;
                    Pm25 pm25 = JsonConvert.DeserializeObject<Pm25>(data);
                    Item[] item = pm25.items;
                    Region_Metadata[] regionMetadata = pm25.region_metadata;
                    Console.WriteLine("{0}", item[0].timestamp);
                    Console.WriteLine("{0}", item[0].update_timestamp);
                    Readings reading = item[0].readings;
                    foreach (Region_Metadata rm in regionMetadata)
                    {
                        if (rm.name == "north")
                        {
                            Console.WriteLine("North psi: {0}", reading.pm25_one_hourly.north);
                        }
                        if (rm.name == "south")
                        {
                            Console.WriteLine("South psi: {0}", reading.pm25_one_hourly.south);
                        }
                        if (rm.name == "east")
                        {
                            Console.WriteLine("East psi: {0}", reading.pm25_one_hourly.east);
                        }
                        if (rm.name == "west")
                        {
                            Console.WriteLine("West psi: {0}", reading.pm25_one_hourly.west);
                        }
                        if (rm.name == "central")
                        {
                            Console.WriteLine("Central psi: {0}", reading.pm25_one_hourly.central);
                        }
                    }
                }
            }
        }
        static void DisplayXchange()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.exchangerate-api.com");
                //HTTP GET
                Task<HttpResponseMessage> responseTask = client.GetAsync("/v4/latest/SGD");
                responseTask.Wait();

                // Response
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    string data = readTask.Result;
                    RootObject obj = JsonConvert.DeserializeObject<RootObject>(data);

                    // header

                    foreach (var rates in obj.rates.GetType().GetProperties())
                    {
                        Console.WriteLine("Name: {0} \t Rates: {1}", rates.Name, rates.GetValue((obj.rates)));

                    }


                }
            }

        }

    }




}






