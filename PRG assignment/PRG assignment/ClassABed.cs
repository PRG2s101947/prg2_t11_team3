﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class ClassABed : Bed
    {
        public bool AccompanyingPerson { get; set; }

        public ClassABed(int wardNo, int bedNo, double dailyRate, bool available) : base(wardNo, bedNo, dailyRate, available)
        {


        }

        public override double CalculateCharges(string citizenStatus, int noOfDays)
        {
            if (AccompanyingPerson = true)
            {
                return (base.DailyRate * noOfDays) + (noOfDays * 100);
            }
            else
            {
                return base.DailyRate * noOfDays;
            }

        }

        public override string ToString()
        {
            return base.ToString() + "Bed Class A";
        }
    }
}
