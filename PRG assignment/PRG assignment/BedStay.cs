﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class BedStay
    {
        public DateTime StartBedStay { get; set; }
        public DateTime? EndBedStay { get; set; }
        public Bed Bed { get; set; }

        public BedStay(DateTime startBedStay,  Bed bed, DateTime endBedStay)
        {
            StartBedStay = startBedStay;
            EndBedStay = endBedStay;
            Bed = bed;
        }

        public override string ToString()
        {
            return "Start date" + StartBedStay + " End date" + EndBedStay + "Bed info " + Bed.ToString(); ;
        }
    }
}
