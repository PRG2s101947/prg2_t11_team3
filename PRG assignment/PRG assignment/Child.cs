﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class Child : Patient
    {
        public double CdaBalance { get; set; }

        public Child(string id, string name, int age, char gender, string citizenstat, string status, double cdaBalance) : base(id, name, age, gender, citizenstat, status)
        {
            CdaBalance = cdaBalance;
        }

        public override double CalculateCharges()
        {
            if (CitizenStatus == "SC")
            {
                if (CdaBalance < base.CalculateCharges())

                {
                    double charges = base.CalculateCharges() - CdaBalance;
                    Console.WriteLine("Total charges pending: {0}", charges + CdaBalance);
                    Console.WriteLine("CDA balance: {0}", CdaBalance);
                    Console.WriteLine("To deduct from CDA: {0}", CdaBalance);
                    Console.WriteLine("Sub-Total: {0}", charges);

                    Console.WriteLine("[Press any key to proceed with payment]");
                    Console.ReadKey();
                    Console.WriteLine("${0} has been deducted from CDA", CdaBalance);
                    Console.WriteLine("New CDA balance: 0");
                    Console.WriteLine("Sub-total: {0} has been paid by cash", charges);
                    Console.WriteLine("Payment successful!");
                    return charges;



                }
                else
                {
                    double charges = CdaBalance - base.CalculateCharges();
                    Console.WriteLine("Total charges pending: {0}", base.CalculateCharges());
                    Console.WriteLine("CDA balance: {0}", CdaBalance);
                    Console.WriteLine("To deduct from CDA: {0}", base.CalculateCharges());
                    Console.WriteLine("Sub-Total: {0}", 0);
                    Console.WriteLine("[Press any key to proceed with payment]");
                    Console.ReadKey();
                    Console.WriteLine("${0} has been deducted from CDA", base.CalculateCharges());
                    Console.WriteLine("New CDA balance: {0}", charges);
                    Console.WriteLine("Payment successful!");
                    return 0.0;
                }

            }
            else
            {
                return base.CalculateCharges();
            }



        }

        public override string ToString()
        {
            return base.ToString() + "Balance" + CdaBalance;
        }
    }
}

