﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class Senior : Patient
    {
        public Senior(string id, string name, int age, char gender, string citizenstat, string status) : base(id, name, age, gender, citizenstat, status)
        {

        }

        public override double CalculateCharges()
        {
            return (base.CalculateCharges() / 2);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
