﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    abstract class Patient
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public char Gender { get; set; }
        public string CitizenStatus { get; set; }
        public string Status { get; set; }
        public Stay Stay { get; set; }

        public Patient(string id, string name, int age, char gender, string citizenstat, string status)
        {
            Id = id;
            Name = name;
            Age = age;
            Gender = gender;
            CitizenStatus = citizenstat;
            Status = status;
        }

        public virtual double CalculateCharges()
        {
            double Charge = 0;

            foreach (BedStay i in Stay.BedStayList)
            {

                int duration = Convert.ToInt32((Convert.ToDateTime(i.EndBedStay) - i.StartBedStay).TotalDays);

                Charge = i.Bed.CalculateCharges(CitizenStatus, duration);


            }
            return Charge;
        }

        public override string ToString()
        {

            Console.WriteLine("{0,-10}{1,10}{2,10}{3,10}{4,10}{5,10}", Name, Id, Age, Gender, CitizenStatus, Status);
            return null;

        }
    }
}
