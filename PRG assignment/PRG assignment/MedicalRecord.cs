﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    class MedicalRecord
    {
        public string Diagnosis { get; set; }
        public double Temperature { get; set; }
        public DateTime DatetimeEntered { get; set; }

        public MedicalRecord(string diagnosis, double temperature, DateTime datetimeEntered)
        {
            Diagnosis = diagnosis;
            Temperature = temperature;
            DatetimeEntered = datetimeEntered;
        }

        public override string ToString()
        {
            return "Diagnosis" + Diagnosis + "Temperature" + Temperature + "Date time entered" + DatetimeEntered;
        }
    }
}
