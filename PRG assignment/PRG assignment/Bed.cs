﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{
    abstract class Bed
    {
        public int WardNo { get; set; }
        public int BedNo { get; set; }
        public double DailyRate { get; set; }
        public bool Available { get; set; }

        public Bed(int wardNo, int bedNo, double dailyRate, bool available)
        {
            WardNo = wardNo;
            BedNo = bedNo;
            DailyRate = dailyRate;
            Available = available;
        }

        public abstract double CalculateCharges(string citizenStatus, int noOfDays);

        public override string ToString()
        {
            return "Ward No" + WardNo + "Bed No" + BedNo + "Daily Rate" + DailyRate + "Availablity" + Available;
        }


    }
}
