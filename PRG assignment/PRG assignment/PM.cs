﻿
//============================================================
// Student Number	: S10194712H, S10198645J
// Student Name	: Shane-Rhys Chua, Marcus Cheong Tze Fung
// Module  Group	: P11
//============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG_assignment
{

    public class Pm25
    {
        public Region_Metadata[] region_metadata { get; set; }
        public Item[] items { get; set; }
        public Api_Info api_info { get; set; }
    }

    public class Api_Info
    {
        public string status { get; set; }
    }

    public class Region_Metadata
    {
        public string name { get; set; }
        public Label_Location label_location { get; set; }
    }

    public class Label_Location
    {
        public float latitude { get; set; }
        public float longitude { get; set; }
    }

    public class Item
    {
        public DateTime timestamp { get; set; }
        public DateTime update_timestamp { get; set; }
        public Readings readings { get; set; }
    }

    public class Readings
    {
        public Pm25_One_Hourly pm25_one_hourly { get; set; }
    }

    public class Pm25_One_Hourly
    {
        public int west { get; set; }
        public int east { get; set; }
        public int central { get; set; }
        public int south { get; set; }
        public int north { get; set; }
    }

}
